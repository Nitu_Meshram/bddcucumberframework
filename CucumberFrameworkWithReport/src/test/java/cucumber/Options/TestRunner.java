package cucumber.Options;

import org.junit.runner.RunWith;

import com.aventstack.extentreports.ExtentReports;
import io.cucumber.plugin.Plugin;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith (Cucumber.class)
@CucumberOptions(features="src/test/java/features", glue= {"stepDefinitions"},
plugin= {"pretty", "html:target/cucumber.html", "json:target/cucumber.json", "junit:target/cucumber.xml"},
monochrome=true, //display console output in proper readable format
dryRun=false) //check mapping is proper between feature file and step definition file

// plugin={"usage"}// plugin={"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:./Extent-Report/extent.html"})


public class TestRunner { 
	
}
