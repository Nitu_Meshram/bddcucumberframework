package stepDefinitions;

import commonMethods.CommonFunction;
import endpoints.Delete_endpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import utilityCommonMethods.Manage_api_logs;
import utilityCommonMethods.Manage_directory;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

public class DeleteStepDefinition {
	static String endpoint;
	static int statusCode;
	static String responseBody;
	static File logDirectory;
	
	@Given("Valid Delete request endpoint")
	public void valid_delete_request_endpoint() {
		logDirectory=Manage_directory.create_log_directory("DeleteLogDir");
		endpoint=Delete_endpoint.delete_endpoint();
		}
	@When("Send the Delete request")
	public void send_the_delete_request() {
	    statusCode=CommonFunction.delete_statuscode(endpoint);
	    	}
	@Then("Validate Delete status code")
	public void validate_delete_status_code() throws IOException {
	 Assert.assertEquals(statusCode,204);
	 System.out.println("Delete Operation is Successful");
	 Manage_api_logs.create_evidence(logDirectory, "DeleteTc1Log", endpoint, null, null);
	}

}
