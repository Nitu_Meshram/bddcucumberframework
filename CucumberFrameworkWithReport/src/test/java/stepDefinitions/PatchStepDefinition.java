package stepDefinitions;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import commonMethods.CommonFunction;
import endpoints.Patch_endpoint;
import endpoints.Post_endpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

import org.testng.Assert;
import io.cucumber.java.en.Then;
import request_repository.Patch_request_repository;
import request_repository.Post_request_repository;
import utilityCommonMethods.Manage_api_logs;
import utilityCommonMethods.Manage_directory;

public class PatchStepDefinition {
	static String endpoint;
	static String requestBody;
	static int statusCode;
	static String responseBody;
	static File logDirectory;

	@Given("Enter NAME and JOB in Patch request body")
	public void enter_name_and_job_in_patch_request_body() throws IOException {
		logDirectory=Manage_directory.create_log_directory("PatchLogDir");
		endpoint = Patch_endpoint.patch_endpoint_tc1();
		requestBody = Patch_request_repository.patch_request_tc1();
	}

	@When("Send the Patch request with payload")
	public void send_the_patch_request_with_payload() throws IOException {
		statusCode = CommonFunction.patch_statuscode(requestBody, endpoint);
		responseBody = CommonFunction.patch_responsebody(requestBody, endpoint);
		System.out.println(responseBody);
		Manage_api_logs.create_evidence(logDirectory, "PatchTc1Log", endpoint, requestBody, responseBody);
	}

	@Then("Validate Patch status code")
	public void validate_patch_status_code() {
		Assert.assertEquals(statusCode, 200);
	}

	@Then("Validate Patch response body parameters")
	public void validate_patch_response_body_parameters() {
		JsonPath jsprequest = new JsonPath(requestBody);
		String req_name = jsprequest.getString("name");
		String req_job = jsprequest.getString("job");
		LocalDateTime date = LocalDateTime.now();
		String sysdate = date.toString().substring(0, 10);
		JsonPath jspresponse = new JsonPath(responseBody);
		String res_name = jspresponse.getString("name");
		String res_job = jspresponse.getString("job");
		String res_date = jspresponse.getString("updatedAt").substring(0, 10);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(sysdate, res_date);
		System.out.println("Response Body Validation Successful"+"\n");
	}
}
