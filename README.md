# BDDCucumberFramework

My BDDCucumberFramework framework consists of following components, 
1. TestRunner 
2. Feature.file, 
3. StepDefinitions, 
4. Common Function, 
5. libraries and  
6. Reports.

TestRunner: --> It is used as a driving mechanism to execute the test cases.It consists of @RunWith and @Cucumber.Options annotation which provides the location of Feature file and stepdefinitions file. Test cases can also be executed on mvn CLI tool using mvn test command.

Feature.file: --> A feature file start with the keyword Feature, followed by Scenario and has .feature extension. It used to provide high level description of our test cases. It consists of keywords like Feature, Given, When, and, Then, Scenario, Examples etc.

StepDefinitions: --> It is used to map the plain text written in feature file with our functional java code. It creates a wrapper on top of our java code. Every step in feature file is map with the method in stepdefinition file. 

Common Function: --> I segregated it into two categories API related common function and Utilities related common function.  In API_common_function I have created methods to trigger the API, fetch the responsebody and extract response status code.

Utilities_common_function, contains methods for creating API logs (endpoint, requestbody, responsebody) once the API is executed and saving them into seperate text files. Second utility is creating a directory if it does not exist and deleting and then creating it if it already exist. Then I have third utility to read data from excel file using apache poi library. 

Libraries: --> I have added Rest Assured Jars for triggering the API, extarcting statuscode, extracting responsebody, parsing response body using JsonPath. Having TestNG library for validating the response parameters using assert class. Then also used apache poi libraries to read and write data from excel file, Junit library for executing the API.

Reports: --> Cucumber has its own reporting mechanism, using plugin attribute in testRunner class I have created html, Json, Xml reports apart from the in built index.html report, Junit console output.

Hooks and tags: --> Also used attributes like tags to execute desired scenarios from a huge Feature file. Used Hooks @Before and @After to execute selected methods scenario wise and also decided flow of execution of methods by using hooks with attribute order. 
