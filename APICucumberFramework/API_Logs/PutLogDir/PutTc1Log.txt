Endpoint is :https://reqres.in/api/users/2

Request Body is :{
    "name": "Mishka",
    "job": "QAMgr"
}

Response Body is :{"name":"Mishka","job":"QAMgr","updatedAt":"2024-01-04T06:42:59.793Z"}