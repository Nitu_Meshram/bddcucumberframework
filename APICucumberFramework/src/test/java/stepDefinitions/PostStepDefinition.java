package stepDefinitions;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import commonMethods.CommonFunction;
import endpoints.Post_endpoint;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;

import org.junit.Test;
import org.testng.Assert;
import request_repository.Post_request_repository;
import utilityCommonMethods.Manage_api_logs;
import utilityCommonMethods.Manage_directory;

public class PostStepDefinition {
	static String requestBody;
	static String endpoint;
	static int statusCode;
	static String responseBody;
	static File logDir;

	@Given("Enter {string} and {string} in post payload")
	public void enter_and_in_post_payload(String reqName, String reqJob) throws IOException {
		logDir = Manage_directory.create_log_directory("PostLogDir");
		endpoint = Post_endpoint.post_endpoint_tc1();
		requestBody = Post_request_repository.post_request_tc1();
	}

	@When("Send the Post request with payload")
	public void send_the_post_request_with_payload() throws IOException {
		statusCode = CommonFunction.post_statuscode(requestBody, endpoint);
		responseBody = CommonFunction.post_responsebody(requestBody, endpoint);
		System.out.println(responseBody);
		Manage_api_logs.create_evidence(logDir, "PostTC1Log", endpoint, requestBody, responseBody);	
	}

	@Then("Validate Post status code")
	public void validate_post_status_code() {
		Assert.assertEquals(statusCode, 201);
	}

	@Then("Validate Post response body parameters")
	public void validate_post_response_body_parameters() {
		JsonPath jsprequest = new JsonPath(requestBody);
		String req_name = jsprequest.getString("name");
		String req_job = jsprequest.getString("job");
		LocalDateTime date = LocalDateTime.now();
		String sysdate = date.toString().substring(0, 10);
		
		JsonPath jspresponse = new JsonPath(responseBody);
		String res_name = jspresponse.getString("name");
		String res_job = jspresponse.getString("job");
		String res_date = jspresponse.getString("createdAt").substring(0, 10);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
	//	Assert.assertEquals(sysdate, res_date);
		System.out.println("Post API Response Body Validation Successful" + "\n\n");	
	}

}
