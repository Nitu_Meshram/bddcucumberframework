package stepDefinitions;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;
import commonMethods.CommonFunction;
import endpoints.Put_endpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import request_repository.Put_request_repository;
import utilityCommonMethods.Manage_api_logs;
import utilityCommonMethods.Manage_directory;

public class PutStepDefinition {
	static String endpoint;
	static String requestBody;
	static int statusCode;
	static String responseBody;
	static File logDirectory;

	@Given("Enter {string} and {string} in Put payload")
	public void enter_and_in_put_payload(String reqName, String reqJob) throws IOException {
		logDirectory=Manage_directory.create_log_directory("PutLogDir");
		endpoint = Put_endpoint.put_endpoint_tc1();
		requestBody = "{\r\n"
				+ "    \"name\": \""+reqName+"\",\r\n"
				+ "    \"job\": \""+reqJob+"\"\r\n"
				+ "}";
	}

	@When("Send the Put request with payload")
	public void send_the_put_request_with_payload() throws IOException {
		statusCode = CommonFunction.put_statuscode(requestBody, endpoint);
		responseBody = CommonFunction.put_responsebody(requestBody, endpoint);
		System.out.println(responseBody);
		Manage_api_logs.create_evidence(logDirectory,"PutTc1Log", endpoint, requestBody, responseBody);
	}

	@Then("Validate Put status code")
	public void validate_put_status_code() {
		Assert.assertEquals(statusCode, 200);
	}

	@Then("Validate Put response body parameters")
	public void validate_put_response_body_parameters() {
		JsonPath jsprequest = new JsonPath(requestBody);
		String req_name = jsprequest.getString("name");
		String req_job = jsprequest.getString("job");
		LocalDateTime date = LocalDateTime.now();
		String sysdate = date.toString().substring(0, 10);
		JsonPath jspresponse = new JsonPath(responseBody);
		String res_name = jspresponse.getString("name");
		String res_job = jspresponse.getString("job");
		String res_date = jspresponse.getString("updatedAt").substring(0, 10);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
	//	Assert.assertEquals(sysdate, res_date);
		System.out.println("Put API Response Body Validation Successful"+"\n\n");
	}

}
