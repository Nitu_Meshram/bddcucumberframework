package stepDefinitions;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import commonMethods.CommonFunction;
import endpoints.Post_endpoint;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import request_repository.Post_request_repository;
import utilityCommonMethods.Manage_api_logs;
import utilityCommonMethods.Manage_directory;

public class stepDefinitionWithHooks {
	static String requestBody;
	static String endpoint;
	static int statusCode;
	static String responseBody;
	static File logDir;
 
	@Before(order=1)                                                                                  //Global hook executed before every scenario 
	public void onStart() {
		System.out.println("onStart method invoked.............");
	}

	@Before(order=0)                                                                                  //local hook executed only before first scenario                                  
	public void testSetUp() {
		System.out.println("Pre Condition.................");
		logDir = Manage_directory.create_log_directory("PostLogDir");
	}
 
	@After(order=3)                                                                                            //local hook executed only after first scenario
	public void testTearDown() throws IOException {
		System.out.println("Post condition.................");
		Manage_api_logs.create_evidence(logDir, "PostTC1Log", endpoint, requestBody, responseBody);
		
	}

	@After (order=2)                                                                                            //Global hook executed after every scenario 
	public void onTestSuccess() {
		System.out.println("API tested successfully........."+"\n\n");
	}

	@Given("Enter required parameters in post request body")
	public void enter_required_parameters_in_post_request_body() throws IOException {
		endpoint = Post_endpoint.post_endpoint_tc1();
		requestBody = Post_request_repository.post_request_tc1();
	}

	@When("Send the Post API request with given payload")
	public void send_the_post_api_request_with_given_payload() throws IOException {
		statusCode = CommonFunction.post_statuscode(requestBody, endpoint);
		responseBody = CommonFunction.post_responsebody(requestBody, endpoint);
		System.out.println(responseBody);
	}

	@Then("Post API status code validation")
	public void post_api_status_code_validation() {
		Assert.assertEquals(statusCode, 201);
	}

	@Then("Post response body validation")
	public void post_response_body_validation() {
		JsonPath jsprequest = new JsonPath(requestBody);
		String req_name = jsprequest.getString("name");
		String req_job = jsprequest.getString("job");
		LocalDateTime date = LocalDateTime.now();
		String sysdate = date.toString().substring(0, 10);

		JsonPath jspresponse = new JsonPath(responseBody);
		String res_name = jspresponse.getString("name");
		String res_job = jspresponse.getString("job");
		String res_date = jspresponse.getString("createdAt").substring(0, 10);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		// Assert.assertEquals(sysdate, res_date);
		System.out.println("Post API Response Body Validation Successful");
	}

}
