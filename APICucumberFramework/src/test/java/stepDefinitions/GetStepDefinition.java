package stepDefinitions;

import commonMethods.CommonFunction;
import endpoints.Get_endpoint;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import utilityCommonMethods.Manage_api_logs;
import utilityCommonMethods.Manage_directory;

import java.io.File;
import java.io.IOException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.testng.Assert;

public class GetStepDefinition {
	static String endpoint;
	static int statusCode;
	static String responseBody;
	File logDirectory;

	@Given("Valid Get request Base Url")
	public void valid_endpoint() {
		logDirectory=Manage_directory.create_log_directory("GetLogDir");
		endpoint = Get_endpoint.get_endpoint();
	}

	@When("Send the Get request")
	public void send_the_get_request() throws IOException {
		statusCode = CommonFunction.get_statuscode(endpoint);
		responseBody = CommonFunction.get_responsebody(endpoint);
		System.out.println(responseBody);
		Manage_api_logs.create_evidence(logDirectory, "GetTc1Log", endpoint, null, responseBody);
	}

	@Then("Validate Get status code")
	public void validate_get_status_code() {
		Assert.assertEquals(statusCode, 200);
	}

	@Then("Validate Get response body parameters")
	public void validate_get_response_body_parameters() {
		JSONObject joRes = new JSONObject(responseBody);
		JSONArray dataArray = joRes.getJSONArray("data");
		int count = dataArray.length();
		int id[] = { 1, 2, 3, 4, 5, 6 };
		String fname[] = { "George", "Janet", "Emma", "Eve", "Charles", "Tracey" };
		String lname[] = { "Bluth", "Weaver", "Wong", "Holt", "Morris", "Ramos" };
		String email[] = { "george.bluth@reqres.in", "janet.weaver@reqres.in", "emma.wong@reqres.in",
				"eve.holt@reqres.in", "charles.morris@reqres.in", "tracey.ramos@reqres.in" };

		for (int i = 0; i < count; i++) {
			int exp_id = id[i];
			String exp_fname = fname[i];
			String exp_lname = lname[i];
			String exp_email = email[i];

			int res_id = dataArray.getJSONObject(i).getInt("id");
			String res_fname = dataArray.getJSONObject(i).getString("first_name");
			String res_lname = dataArray.getJSONObject(i).getString("last_name");
			String res_email = dataArray.getJSONObject(i).getString("email");

			Assert.assertEquals(res_id, exp_id);
			Assert.assertEquals(res_email, exp_email);
			Assert.assertEquals(res_fname, exp_fname);
			Assert.assertEquals(res_lname, exp_lname);
		}
		System.out.println("Get API Response Body Validation Successful");
	}

}
