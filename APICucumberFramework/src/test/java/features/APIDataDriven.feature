Feature: Trigger the API 


Scenario Outline: Trigger the Post API request with valid parameters 
	Given Enter "<NAME>" and "<JOB>" in post request body
	When Send the Post API with given payload
	Then Validate Post API status code 
	And Validate Post API response body parameters 

Examples: 
        |NAME| JOB|
        |Neha| QA|
        |Sneha|QA|
        |Riya| SrQA|
        |Raj|QAMgr|
       
Scenario Outline: Trigger the Put API request with valid request parameters 
	Given Enter "<NAME>" and "<JOB>" in Put payload
	When Send the Put request with payload 
	Then Validate Put status code 
	And Validate Put response body parameters 
	
Examples: 
        |NAME| JOB|
        |Naisha| QA|
        |Nimisha|QA|
        |Minisha| SrQA|
        |Mishka|QAMgr|


Scenario Outline: Trigger the Patch API request with valid request parameters 
	Given Enter "<NAME>" and "<JOB>" in Patch payload
	When Send the Patch request with payload 
	Then Validate Patch status code 
	And Validate Patch response body parameters 
	
Examples: 
        |NAME| JOB|
        |abhijit| QA|
        |abhishek|QA|
        |amar| SrQA|
        |aman|QAMgr|
        
        