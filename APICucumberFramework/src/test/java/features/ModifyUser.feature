Feature: Trigger the API 
@putAPI	
Scenario Outline: Trigger the Put API request with valid request parameters 
	Given Enter "<NAME>" and "<JOB>" in Put payload
	When Send the Put request with payload 
	Then Validate Put status code 
	And Validate Put response body parameters 
	
Examples: 
        |NAME| JOB|
        |Naisha| QA|
        |Nimisha|QA|
        |Minisha| SrQA|
        |Mishka|QAMgr|