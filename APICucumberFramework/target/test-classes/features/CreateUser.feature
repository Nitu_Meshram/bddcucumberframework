Feature: Trigger the API 
@postAPI
Scenario Outline: Trigger the Post API request with valid request parameters 
	Given Enter "<NAME>" and "<JOB>" in post payload
	When Send the Post request with payload
	Then Validate Post status code 
	And Validate Post response body parameters 

Examples: 
        |NAME| JOB|
        |Naina| QA|
        |Sunaina|QA|
        |Raima| SrQA|
        |Ritvik|QAMgr|
        	
	