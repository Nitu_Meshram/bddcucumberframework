Feature: Trigger the API 
@patchAPI	
Scenario: Trigger the Patch API request with valid request parameters 
	Given Patch API with valid endpoint, body and headers
	When Send the Patch request with payload 
	Then Validate Patch status code 
	And Validate Patch response body parameters 
