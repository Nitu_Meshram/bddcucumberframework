@Tag
Feature: Trigger the API 

@First
Scenario: Post API request trigger with valid parameters 
	Given Enter required parameters in post request body
	When Send the Post API request with given payload
	Then Post API status code validation
	And Post response body validation
	
@Second
Scenario: Trigger the Get API 
	Given Valid Get request Base Url
	When Send the Get request 
	Then Validate Get status code 
	And Validate Get response body parameters 
	
